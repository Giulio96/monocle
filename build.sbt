name := "monocleproj"

version := "3.0.0"

scalaVersion := "2.13.7"

libraryDependencies ++= Seq(
  "dev.optics"  %%  "monocle-core"    % "3.0.0",
  "dev.optics"  %%  "monocle-generic" % "3.0.0",
  "dev.optics"  %%  "monocle-macro"   % "3.0.0",
  "dev.optics"  %%  "monocle-state"   % "3.0.0",
  "dev.optics"  %%  "monocle-refined" % "3.0.0",
  "dev.optics"  %%  "monocle-unsafe"  % "3.0.0",
  "dev.optics"  %%  "monocle-law"     % "3.0.0" % "test",
  "com.github.nscala-time" %% "nscala-time" % "2.30.0"
)
val circeVersion = "0.14.1"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

scalacOptions in Global += "-Ymacro-annotations"