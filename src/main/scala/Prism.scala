import monocle.Monocle.doubleToInt
import monocle.Prism
import monocle.macros.{GenIso, GenPrism}


sealed trait Json
case object JNull extends Json
case class JStr(v: String) extends Json
case class JNum(v: Double) extends Json
case class JObj(v: Map[String, Json]) extends Json

object PrismEx extends App{

  /*
  Un prims (Prism[S,A]) viene utilizzato per selezionare parti di un oggetto (Coproduct).
  In questo caso vogliamo selezionare solamente gli elementi di tipo Json con un costruttore che accetta una stringa
  Un altro utilizzo è equivalente è dato dal definire un Prism tramite un pattern matcher sul costruttore di un oggetto,
  come nel caso di jStr2.
  In questo caso il prism che abbiamo creato hanno implicitamente definite le seguenti funzioni:

    - getOption: Json => Option[String]
    - reverseGet (aka apply): String => Json
   */

  val jStr = Prism[Json, String]{
    case JStr(v) => Some(v)
    case _       => None
  }(JStr)

  val jStr2 = Prism.partial[Json, String]{case JStr(v) => v}(JStr)

  println(jStr("Hello"))                      //JStr(Hello)
  println(jStr.getOption(JStr("Hello")))      //Some(Hello)
  println(jStr.getOption(JNum(3.2)))          //None

  /*
  Anche in questo caso possiamo modificare il contenuto di un oggetto  JStr
   */

  println(jStr.replace("Bar")(JStr("Hello")))       // res3: Json = JStr(v = "Bar")
  println(jStr.modify(_.reverse)(JStr("Hello")))    // res4: Json = JStr(v = "olleH")


  println(jStr.replace("Bar")(JNum(10)))            //JNum(10.0)
  println(jStr.modify(_.reverse)(JNum(10)))         //JNum(10.0)

  /*
  I seguenti metodi vengono utilizzati nel caso in cui si voglia tenere conto del success o failure della modifica
   */
  println(jStr.modifyOption(_.reverse)(JStr("Hello")))     //Some(value = JStr(v = "olleH"))
  println(jStr.modifyOption(_.reverse)(JNum(10)))           //None

  /*
  è possibile anche in questo caso comporre dei prism assieme. In questo caso creiamo un prism che converte un Json in
  double, e poi convertiamo il double in Int
   */
  val jNum: Prism[Json, Double] = Prism.partial[Json, Double]{case JNum(v) => v}(JNum)
  val jInt: Prism[Json, Int] = jNum.andThen(doubleToInt)


  println(jInt(5))                              //JNum(5.0)
  println(jInt.getOption(JNum(5.0)))            //Some(5)
  println(jInt.getOption(JNum(5.2)))            //
  println(jInt.getOption(JStr("Hello")))        //None


  /*
  è possibile generare un prism tramite GenPrism
   */

  val rawJNum: Prism[Json, JNum] = GenPrism[Json, JNum]

  println(rawJNum.getOption(JNum(4.5)))
  println(rawJNum.getOption(JStr("Hello")))

  val jNum1: Prism[Json, Double] = GenPrism[Json, JNum].andThen(GenIso[JNum, Double])
  val jNull: Prism[Json, Unit] = GenPrism[Json, JNull.type].andThen(GenIso.unit[JNull.type])

  println(jNum1.getOption(JNum(4.1)))
  println(jNull.getOption(JStr("ciao")))


  /*
  I seguenti metodi possono essere utilizzati per controllare la corretta generazione di un prism
   */

  def partialRoundTripOneWay[S, A](p: Prism[S, A], s: S): Boolean =
    p.getOption(s) match {
      case None    => true // nothing to prove
      case Some(a) => p.reverseGet(a) == s
    }

  def partialRoundTripOtherWay[S, A](p: Prism[S, A], a: A): Boolean =
    p.getOption(p.reverseGet(a)) == Some(a)

  println(partialRoundTripOtherWay(jNum1, 0.5))
  println(partialRoundTripOneWay(jNum1, JNum(5.0)))
}
