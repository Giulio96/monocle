import monocle._
import monocle.law.IsoLaws
import monocle.macros.GenIso


case class Person(name: String, age: Int)
case class MyString(s: String)



object IsoEx extends App{


  //Iso[S,A] -> Iso[Person, Tuple[String,Int]]
  /*

   */
  val personToTuple = Iso[Person, (String, Int)](p => (p.name, p.age)) (t => Person(t._1, t._2))

  println(personToTuple.get(Person("Zoe", 25)) )    //(Zoe,25)
  println(personToTuple.reverseGet(("Zoe", 25)))    //Person(Zoe,25)

  //  println(personToTuple(Person("Giulio",25)))
  println(personToTuple(("Linda",25)))

  def listToVector[A] = Iso[List[A], Vector[A]](_.toVector)(_.toList)
  def vectorToList[A] = listToVector[A].reverse

  println(vectorToList.reverseGet(List(1,2,3)) == listToVector.get(List(1,2,3)))

  println(listToVector(Vector(1, 2, 3)))   //Vector(1, 2, 3)
  println(vectorToList(List(1, 2, 3)))     //List(1, 2, 3)

  val stringToList = Iso[String, List[Char]](_.toList)(_.mkString(""))

  println(stringToList.get("Hello"))      // List('h','e','l','l','o')
  println(stringToList.reverseGet(List('h','e','l','l','o')))  // hello

  /*
  La funzione modify (A => B) -> (List[Char] => List[Char]) prende come argomento la stringa passata come secondo parametro,
  convertita in List[Char] dall'Iso
   */
  println(stringToList.modify(_.tail)("Hello"))


  var s = GenIso[MyString, String].get(MyString("Hello"))

  /*
  GenIso.fields consente di ottenere una sequenza di campi di un certo oggetto
   */
  var t = GenIso.fields[Person]
  t.get(Person("Giulio",25))
  println(s)
  println(t)


  def roundTripOneWay[S, A](i: Iso[S, A], s: S): Boolean =
    i.reverseGet(i.get(s)) == s

  def roundTripOtherWay[S, A](i: Iso[S, A], a: A): Boolean =
    i.get(i.reverseGet(a)) == a

  println(roundTripOneWay(personToTuple, Person("Giulio",25)))
  println(roundTripOtherWay(personToTuple, ("Giulio",25)))

}
