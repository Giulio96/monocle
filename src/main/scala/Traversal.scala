
import alleycats.std.all.alleycatsStdInstancesForMap
import cats.Applicative
import monocle.Traversal
import cats.implicits._

case class Point(id: String, x: Int, y: Int)




object TraversalEx extends App{

  /*
  Un traversal amplia il concetto di optics che permette di effettuare un focus da un tipo S in 0 - n tipi A.
  L'esempio più pratico è quello di effettuare un focus all'interno di una collection di oggetti contenuti ad esempio
  in una lista o in un vettore.
  Nei seguenti esempi ci sono metodi che lavorano su liste
   */


  val xs = List(1,2,3,4,5)
  val eachL = Traversal.fromTraverse[List, Int]
  println(eachL.replace(0)(xs))          // res0: List[Int] = List(0, 0, 0, 0, 0)
  println(eachL.modify(_ + 1)(xs))       // res1: List[Int] = List(2, 3, 4, 5, 6)

  println(eachL.getAll(xs))             //  List(1,2,3,4,5)
  println(eachL.headOption(xs))         //  Some(1)
  println(eachL.find(_ > 3)(xs))        //  Some(4)
  println(eachL.all(_ % 2 == 0)(xs))    //  false



  /*
  In questo caso stiamo dichiarando un traversal che consente di costruire un Point (string, int, int)
  Con replace(5) assegnamo x = 5 e y = 5 al Point
   */
  val points = Traversal.apply2[Point, Int](_.x, _.y)((x, y, p) => p.copy(x = x, y = y))
  println(points.replace(5)(Point("bottom-left",0,0)))    //Point(bottom-left,5,5)


  /*
  In questo caso definiamo una funzione filter basata su un certo tipo di predicato che consente di effettuare certe modifiche
  ad un oggetto (elementi lista) solo se soddisfano un certo predicato
   */
  def filterKey[K, V](predicate: K => Boolean): Traversal[Map[K, V], V] =
    new Traversal[Map[K, V], V]{
      def modifyA[F[_]: Applicative](f: V => F[V])(s: Map[K, V]): F[Map[K, V]] =
        s.map{ case (k, v) =>
          k -> (
            if(predicate(k)) f(v)
            else v.pure[F]
            )
        }.sequence
    }


  val m = Map(1 -> "one", 2 -> "two", 3 -> "three", 4 -> "Four")

  val filterEven = filterKey[Int, String](_ % 2 == 0)

  println(filterEven.modify(_.toUpperCase)(m))
}
