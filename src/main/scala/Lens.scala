//https://www.optics.dev/Monocle/docs/optics/iso

import monocle.Lens
import monocle.macros.{GenLens, Lenses}

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import monocle.macros.syntax.lens._

case class Address(streetNumber: Int, streetName: String)
case class PersonWithAddress(name: String, age: Int, address: Address)
case class IdentityCard(number:Int, person: PersonWithAddress)
object LensEx extends App{

  /*
  I lens sono optics utilizzati per zoommare all'interno di un oggetto, (lista, mappa, tupla...)
  Un Lens è definito da 2 parametri (Lens[S,A]) dove S è il prodotto e A è l'elemento da visualizzare al suo interno.
   */

  val getStreetNumber = Lens[Address,Int](_.streetNumber)(n => a => a.copy(streetNumber = n))

  /*
  è possibile automatizzare la creazione di Lens tramite GenLens
   */

  val streetN2 = GenLens[Address](_.streetNumber)

  val address = Address(10, "High Street")

  println(getStreetNumber.get(address))   //10

  /*
  è possibile modificare il target di un lens tramite replate, che accetta il singolo valore da modificare
  oppure modify che in questo caso accetta una funzione A => A
   */
  println(getStreetNumber.replace(7)(address))    //Address(7,High Street)

  println(getStreetNumber.modify(_ + 1)(address)) //Address(11,High Street)

  def neighbors(n: Int): List[Int] =
    if(n > 0) List(n - 1, n + 1) else List(n + 1)

  /*
  La funzione modifyF consente di specificare il valore del parametro target A all'interno di una funzione che ad esempio,
  modifica il target di F assegnando una lista come in questo caso (l'oggetto deve avere lo stesso tipo di A)
   */
  println(getStreetNumber.modifyF(neighbors)(address))    //List(Address(9,High Street), Address(11,High Street))

  println(getStreetNumber.modifyF(neighbors)(Address(0, "High Street")))    //List(Address(1,High Street))


  /*
    La funzione modifyF è particolarmente utile nei casi in cui un risultato dovesse essere modificato da una funzione asincrona
   */
  def updateNumber(n: Int): Future[Int] = Future.successful(n + 1)
  Thread.sleep(500)
  println(getStreetNumber.modifyF(updateNumber)(address))


  /*
  Le funzioni dichiarate come Lens permettono di modificare "deeply" un oggetto
   */

  val john = PersonWithAddress("John", 20, Address(10, "High Street"))

  val address2 = GenLens[PersonWithAddress](_.address)
  println(address2.andThen(getStreetNumber).get(john))
  println(address2.andThen(getStreetNumber).replace(2)(john))

  /*
  è possibile anche comporre assieme più lenses, creando una funzione di update che modifica più campi di un oggetto
   */
  val giulio = Person("Giulio",25)
  val p = GenLens[Person](_.name).replace("Mike") compose GenLens[Person](_.age).replace(21)
  println(p.apply(giulio))

  /*
  è possibile modificare più proprietà di un oggetto in-line
   */
  println(john.focus(_.name).replace("Mike").focus(_.age).modify(_ + 1))



  /*
  Potrebbe essere comodo riuscire a modificare un valore interno ad un oggetto,  per fare ciò possiamo comporre un Prism
  con un Lens utilizzando il costrutto some
   */
  case class B(c: Int)
  case class A(b: Option[B])

  val c = GenLens[B](_.c)
  val b = GenLens[A](_.b)

  println(b.some.andThen(c).getOption(A(Some(B(1)))))   //Some(1)


  /*
  I Lens possono essere dichiarati per modificare parametri ad arbitrari livelli di profondità
   */

  val ic = IdentityCard(13, john)
  var myLens = GenLens[IdentityCard](_.person.address.streetName)

  println(myLens.replace("Culo")(ic))


  /*
  Tramite annotations possiamo definire una Lens già predisposta per modificare tutti i parametri di un oggetto
  Necessario aggiungere :
    scalacOptions in Global += "-Ymacro-annotations"
   in build.sbt
   */
  @Lenses("_")
  case class MyPoint(x: Int, y: Int)
  val point = MyPoint(5, 3)

  println(MyPoint._x.replace(4)(point))
  println(MyPoint._y.replace(20)(point))


  /*
  Di seguito i metodi per verificare la corretta creazione dei Lens
   */
  def getReplace[S, A](l: Lens[S, A], s: S): Boolean =
    l.replace(l.get(s))(s) == s

  def replaceGet[S, A](l: Lens[S, A], s: S, a: A): Boolean =
    l.get(l.replace(a)(s)) == a

  println(getReplace(myLens, ic))
  println(replaceGet(myLens, ic, "ciao"))

}
