package GraphQL.db

import com.github.nscala_time.time.Imports.{DateTime, DateTimeFormat}
import io.circe.generic.JsonCodec
import io.circe.{Decoder, Encoder, Json, KeyDecoder, KeyEncoder, parser}

import java.time.Instant
import scala.util.Try


//@JsonCodec case class Address (number:Int, streetName:String)
@JsonCodec case class User(id:Int, name: String, surname: String, age: Int)
@JsonCodec case class Post(id:Int, text: String, userId: Int, date: Instant)
@JsonCodec case class Comment(id:Int, text:String, userId:Int, postId:Int, date: Instant)

@JsonCodec case class UserR(user:User, posts:List[Post], comments:List[Comment])
case class PostR(post: Post, user:User, comments:List[Comment])
case class CommentR(comment:Comment, user:User, post:Post)
