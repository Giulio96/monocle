package GraphQL.db

import GraphQL.application.Configuration
import GraphQL.application.codecutils.{Codec, CommentCodec, CommentListCodec, JsonConvert, PostCodec, PostListCodec, UserCodec, UserListCodec}

object Database{

  implicit val userCodec : Codec[User] = new UserCodec
  implicit val userListCodec : Codec[List[User]] = new UserListCodec
  implicit val postCodec : Codec[Post] = new PostCodec
  implicit val postListCodec : Codec[List[Post]] = new PostListCodec
  implicit val commentCodec: Codec[Comment] = new CommentCodec
  implicit val commentListCodec: Codec[List[Comment]] = new CommentListCodec

  val users = JsonConvert.deserializeObject[List[User]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"users.json").mkString)
  val posts = JsonConvert.deserializeObject[List[Post]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"posts.json").mkString)
  val comments = JsonConvert.deserializeObject[List[Comment]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"comments.json").mkString)

  val ur:UserR = UserR(users(0), posts.take(2), comments.take(4))

  val a = ur;
}
