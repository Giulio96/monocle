package GraphQL.application
import GraphQL.application.codecutils.{Codec, CommentCodec, CommentListCodec, JsonConvert, PostCodec, PostListCodec, UserCodec, UserListCodec, UserRCodec}
import GraphQL.db.{Comment, Post, User, UserR}
import io.circe.JsonObject

import java.io.{File, PrintWriter}
import java.time.Instant

object Main extends App{


  implicit val userCodec : Codec[User] = new UserCodec
  implicit val userListCodec : Codec[List[User]] = new UserListCodec
  implicit val postCodec : Codec[Post] = new PostCodec
  implicit val postListCodec : Codec[List[Post]] = new PostListCodec
  implicit val commentCodec: Codec[Comment] = new CommentCodec
  implicit val commentListCodec: Codec[List[Comment]] = new CommentListCodec

  implicit val userRCodec : Codec[UserR] = new UserRCodec

  //  val writer = new PrintWriter(new File(Configuration.pathToDbFiles + "comments.json" ))
//  writer.write(JsonConvert.serializeObject(List(c1,c2,c3,c4,c5,c6)))
//  writer.close()

  val users = JsonConvert.deserializeObject[List[User]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"users.json").mkString)
  val posts = JsonConvert.deserializeObject[List[Post]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"posts.json").mkString)
  val comments = JsonConvert.deserializeObject[List[Comment]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"comments.json").mkString)

  val bindings:List[UserR] = List()

//  users.foreach(u => {
//    val userPosts = posts.filter(p => p.userId == u.id;
//  })

  val ur:UserR = UserR(users(0), posts.take(2), comments.take(4))



  val a = ur;

  println(JsonConvert.serializeObject(ur))
}
