package GraphQL.application.codecutils

trait Codec[A]{
  def toJson(o:A):String
  def fromJson(s:String):A
}

