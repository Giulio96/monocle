package GraphQL.application.codecutils

import GraphQL.db.User
import io.circe.syntax.EncoderOps
import io.circe.{Json, parser}

case class UserListCodec() extends Codec[List[User]]{
  override def toJson(l: List[User]): String = {
    l.asJson.toString()
  }

  override def fromJson(s: String): List[User] = {
    parser.parse(s) match {
      case Right(obj) => obj.as[List[User]].getOrElse(Json.Null).asInstanceOf[List[User]]
    }
  }
}

case class UserCodec() extends Codec[User] {
  override def toJson(o: User): String = {
    o.asJson.toString()
  }
  override def fromJson(s: String): User = {
    parser.parse(s) match {
      case Right(obj) => obj.as[User].getOrElse(Json.Null).asInstanceOf[User]
    }
  }
}
