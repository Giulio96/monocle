package GraphQL.application.codecutils

import GraphQL.db.{User, UserR}
import io.circe.syntax.EncoderOps
import io.circe.{Json, parser}

case class UserRCodec() extends Codec[UserR] {
  override def toJson(o: UserR): String = {
    o.asJson.toString()
  }
  override def fromJson(s: String): UserR = {
    parser.parse(s) match {
      case Right(obj) => obj.as[UserR].getOrElse(Json.Null).asInstanceOf[UserR]
    }
  }
}

