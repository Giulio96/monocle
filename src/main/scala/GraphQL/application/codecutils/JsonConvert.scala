package GraphQL.application.codecutils

object JsonConvert {
  def serializeObject[A](o: A)(implicit codec: Codec[A]): String = {
    codec.toJson(o)
  }

  def deserializeObject[A](s: String)(implicit codec: Codec[A]): A = {
    codec.fromJson(s)
  }
}
