package GraphQL.application.codecutils

import GraphQL.db.Post
import io.circe.syntax.EncoderOps
import io.circe.{Json, parser}


case class PostListCodec() extends Codec[List[Post]]{
  override def toJson(l: List[Post]): String = {
    l.asJson.toString()
  }

  override def fromJson(s: String): List[Post] = {
    parser.parse(s) match {
      case Right(obj) => obj.as[List[Post]].getOrElse(Json.Null).asInstanceOf[List[Post]]
    }
  }
}

case class PostCodec() extends Codec[Post] {

  override def toJson(o: Post): String = {
    o.asJson.toString()
  }
  override def fromJson(s: String): Post = {
    parser.parse(s) match {
      case Right(obj) => obj.as[Post].getOrElse(Json.Null).asInstanceOf[Post]
    }
  }
}
