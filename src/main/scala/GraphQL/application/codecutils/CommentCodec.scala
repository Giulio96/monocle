package GraphQL.application.codecutils

import GraphQL.db.{Comment, Post}
import io.circe.syntax.EncoderOps
import io.circe.{Json, parser}


case class CommentListCodec() extends Codec[List[Comment]]{
  override def toJson(l: List[Comment]): String = {
    l.asJson.toString()
  }

  override def fromJson(s: String): List[Comment] = {
    parser.parse(s) match {
      case Right(obj) => obj.as[List[Comment]].getOrElse(Json.Null).asInstanceOf[List[Comment]]
    }
  }
}


case class CommentCodec() extends Codec[Comment] {

  override def toJson(o: Comment): String = {
    o.asJson.toString()
  }
  override def fromJson(s: String): Comment = {
    parser.parse(s) match {
      case Right(obj) => obj.as[Comment].getOrElse(Json.Null).asInstanceOf[Comment]
    }
  }
}