package GraphQL.application.model

import GraphQL.application.model
import monocle.Iso

import scala.None


sealed trait Gql
case class GqlStr(s:String) extends Gql
case class GqlObj(var parameter:String, var nodes: List[Gql]) extends Gql {}
case object GqlNull extends Gql

object GqlParser{

  def toString(g:Gql): Unit ={

  }

  private val placeholder:String = "[]"

  def print(o:Some[Gql]):String = this.print(o, List(), this.placeholder)

  private def cleanAndGetTokens(s:String): Array[String] ={
    s.replaceAll(" ", "").replace("{","_{_")
      .replace(",","_,_")
      .replace("}","_}_")
      .split("_").
      filter(_ != "")
  }

  def fromString(s:String) = {
    var tokens = this.cleanAndGetTokens(s)
    var m:List[(Int,Gql)] = List()
    var currentIdx = 0
    for(i <- 0 to tokens.length - 1){
      tokens(i) match {
        case "{" => currentIdx = currentIdx + 1
        case "," => ;
        case "}" => currentIdx = currentIdx - 1
        case _ => {
          if(tokens.length > (i + 1) && tokens(i + 1) == "{"){
            m = m :+ (currentIdx, new GqlObj(tokens(i), List()))
          }else{
            m = m :+ (currentIdx, new GqlStr(tokens(i)))
          }
        }
      }
    }
    var y = m;
    var t = this.buildGqlNode(m.tail, (m.head._1, m.head._2.asInstanceOf[GqlObj]), None)
    println(t)
  }

  private def buildGqlNode(tokens:List[(Int,Gql)], fNode:(Int, GqlObj), cNode:Option[(Int, GqlObj)]): Gql ={
    tokens match {
      case h::t => {
        cNode match {
          case None =>
            h._2 match {
              case s:GqlStr =>
                fNode._2.nodes = fNode._2.nodes :+ s; buildGqlNode(t, fNode, None)
              case o:GqlObj =>
                buildGqlNode(t, fNode, Some(h._1, o))
            }
          case go: Some[(Int,GqlObj)] => {
            if(h._1 == go.get._1){
              // MATCH PER GESTIRE ENTRAMBI GQLOBJ e GQLSTR ?
              fNode._2.nodes = fNode._2.nodes :+ cNode.get._2; buildGqlNode(t, fNode, Some(h._1,h._2.asInstanceOf[GqlObj]))
            }else{
              h._2 match {
                case s:GqlStr =>
                  cNode.get._2.nodes = cNode.get._2.nodes :+ s; buildGqlNode(t, fNode, cNode)
                case o:GqlObj =>
                  buildGqlNode(t, cNode.get, Some(h._1, o));
              }
            }
          }
        }
      }
      case _ => fNode._2.nodes = fNode._2.nodes :+ cNode.get._2; fNode._2
    }
  }

  private def print(g:Option[Gql], l:List[Gql], query:String):String = (g,l) match {
    case (o:Some[GqlObj], List()) =>
      this.print(None, o.get.nodes, this.replaceWithContent(query, s"${o.get.parameter} { ${this.placeholder} }"))
    case (None, l:List[Gql]) => l match{
      case (h::Nil) => h match {
        case g_str:GqlStr => this.print(None, List(), this.replaceWithContent(query, s"${g_str.s}"))
        case o:GqlObj => this.print(None, List(), this.replaceWithContent(query, s"${this.print(Some(o))}"))
      }
      case (h::t) => h match {
        case g_str:GqlStr => this.print(None, t, this.replaceWithContent(query, s"${g_str.s}, ${this.placeholder}"))
        case o:GqlObj => this.print(None, t, this.replaceWithContent(query, s"${this.print(Some(o))}, ${this.placeholder}"))
      }
      case _ => query
    }
  }

  private def replaceWithContent(q:String, content:String): String ={
    q.replace(this.placeholder, content)
  }
}




object tryGql extends App{

  val g = GqlObj("query"
            ,List(GqlObj("users", List(
                        GqlStr("name"),
                        GqlStr("age"),
                        GqlObj("posts",List(
                            GqlStr("text"),
                            GqlStr("date"))),
                        GqlObj("comments", List(
                            GqlStr("textCCC"),
                            GqlStr("dateCCC")
                        ))))))


  val g1 = GqlObj("query",
                  List(GqlObj("users",List(
                              GqlStr("name"),
                              GqlObj("posts",List(
                                  GqlStr("text")))
                  ))))

  val gqlIso = Iso[Gql, String](o => GqlParser.print(Some(o)))(s => GqlObj(s, List(GqlObj("users", List(GqlStr("name"))))))


  var s = GqlParser.print(Some(g))
  println(GqlParser.fromString(s))

  println(GqlParser.fromString(GqlParser.print(Some(g1))))

}
