
import monocle.Optional
import monocle.law.OptionalLaws

object OptionalEx extends App{
  /*
  Un optional consente di focalizzare un dato all'interno di un oggetto. Contrariamente ai Lens, in questo caso l'oggetto
  potrebbe non esistere.
  Un Optional[S,A] viene definito tramite il focus di un oggetto A interno ad S.
  In questo caso cerchiamo di focalizzare l'elemento in testa ad una data lista.

   */
 val head = Optional[List[Int], Int] {
    case Nil => None
    case x :: xs => Some(x)
    } { a => {
     case Nil => Nil
     case x :: xs => a :: xs
   }
 }

  val xs = List(1, 2, 3)
  val ys = List.empty[Int]

  println(head.nonEmpty(xs))    //true
  println(head.nonEmpty(ys))    //false

  /*
  Tramite la funzione getOrModify possiamo farci restituire l'oggetto target nel caso in cui esista (right o left)
   */

  println(head.getOrModify(xs))   //  Right(value = 1)
  println(head.getOrModify(ys))   // Left(value = List())


  /*
  Tramite la funzione replace possiamo modificare il valore target dell'optional
   */

  println(head.replace(5)(xs)) //  List(5, 2, 3)
  println(head.replace(5)(ys)) //  List()

  /*
  Anche in questo caso potremmo applicare la funzione modify
   */

  println(head.modify(_ + 1)(xs))


  /*
  Con modifyOption possiamo invece sapere se la modifica ha avuto successo o meno
   */

  println(head.modifyOption(_ + 1)(xs)) //Some(List(2, 2, 3))
  println(head.modifyOption(_ + 1)(ys)) //None

  class OptionalLaws[S, A](optional: Optional[S, A]) {

    def getOptionSet(s: S): Boolean =
      optional.getOrModify(s).fold(identity, optional.replace(_)(s)) == s

    def setGetOption(s: S, a: A): Boolean =
      optional.getOption(optional.replace(a)(s)) == optional.getOption(s).map(_ => a)

  }

}
